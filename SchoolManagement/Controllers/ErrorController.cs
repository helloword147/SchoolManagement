﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagement.Controllers
{
    public class ErrorController : Controller
    {
        private ILogger<ErrorController> logger;

        public ErrorController(ILogger<ErrorController> logger)
        {
            this.logger = logger;
        }
        //[Route("Error/{statusCode}")]
        //public IActionResult HttpStatusCodeHandler(int statuscode)
        //{
        //    switch (statuscode)
        //    {
        //        case 404:
        //            ViewBag.ErrorMessage = "抱歉，请求的页面的不存在";
        //            break;
        //    }
        //    return View("NotFound");
        //}
        //[AllowAnonymous]

        public IActionResult Error()
        {
            //获取异常详情信息
            var exceptionHandlerPathFeature =
                    HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            //LogError() 方法将异常记录作为日志中的错误类别记录
            logger.LogError($"路径 {exceptionHandlerPathFeature.Path} " +
                $"产生了一个错误{exceptionHandlerPathFeature.Error}");
            return View("Error");
        }
        [Route("Error/{statusCode}")]
        public IActionResult HttpStatusCodeHandler(int statusCode)
        {
            var statusCodeResult = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            switch (statusCode)
            {
                case 404:
                    ViewBag.ErrorMessage = "抱歉，您访问的页面不存在";
                    logger.LogWarning($"发生一个404错误，路径="+$"{statusCodeResult.OriginalPath} 以及查询字符串"+$"{statusCodeResult.OriginalQueryString}");

                    break;
            }
            return View("NotFound");
        }
    }
}
