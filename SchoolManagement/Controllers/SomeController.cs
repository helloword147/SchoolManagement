﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace SchoolManagement.Controllers
{
    [Authorize(Roles = "Admin,User")]
    public class SomeController : Controller
    {
        public string ABC()
        {
            return "我是方法ABC，只有admin和user角色可访问我";
        }
        [Authorize(Roles = "Admin")]
        public string Xyz()
        {
            return "我是方法xyz,用户admin角色可以访问我";
        }
        [AllowAnonymous]
        public string Anyone()
        {
            return "我是方法anyone，任何人可以访问我";
        }
    }
}
