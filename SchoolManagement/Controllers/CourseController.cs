﻿using Microsoft.AspNetCore.Mvc;
using SchoolManagement.Application.Courses;
using SchoolManagement.Application.Courses.Dtos;
using SchoolManagement.DataRepositories;
using SchoolManagement.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagement.Controllers
{
    public class CourseController : Controller
    {
        private readonly AppDbContext _context;
        private ICourseRepository courseRepository;
        private CourseService _courseService;
        //第一种不推荐，不利于松耦合设计
        //public CourseController(AppDbContext context)
        //{
        //    _context = context;
        //}
        public CourseController(ICourseRepository courseRepository)
        { 
        
        }
        public async Task<IActionResult> Index(GetCourseInput input)
        {
            var models = await _courseService.GetPaginatedResult(input);
            return View(models);
        }
    }
}
