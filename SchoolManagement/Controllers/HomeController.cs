﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SchoolManagement.Models;
using SchoolManagement.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using SchoolManagement.Security.CustomTokenProvider;
using Microsoft.AspNetCore.DataProtection;
using SchoolManagement.Infrastructure.Repositories;
using System.Linq.Dynamic.Core;
using SchoolManagement.Application.Students;
using SchoolManagement.Application.Students.Dtos;
using Microsoft.EntityFrameworkCore;

namespace SchoolManagement.Controllers
{


    public class HomeController : Controller
    {
        
        //private readonly IStudentRepository _studentRepository;
        private readonly IRepository<Student, int> _studentRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ILogger _logger;
        private readonly DataProtectionPurposeStrings _purposeStrings;
        private readonly IDataProtector _protector;
        private readonly IStudentService _studentService;
        //使用构造函数注入的方式注入IStudentRepository
        public HomeController(IRepository<Student, int> studentRepository, IWebHostEnvironment webHostEnvironment
            ,ILogger<HomeController> logger, IDataProtectionProvider dataProtectionProvider, DataProtectionPurposeStrings dataProtectionPurposeStrings,
            IStudentService studentService)
        {
            _studentRepository = studentRepository;
            _webHostEnvironment = webHostEnvironment;
            _logger = logger;
            _studentService = studentService;
            this._protector = dataProtectionProvider.CreateProtector(dataProtectionPurposeStrings.StudentIdRouteValue);
        }

        public async Task<ActionResult> About()
        {
            //获取IQueryable类型的Student,然后通过student.EnrollmentDate进行分组
            var data = from student in _studentRepository.GetAll()
                       group student by student.EnrollmentDate into dateGroup
                       select new EnrollmentDateGroupDto()
                       { 
                       EnrollmentDate=dateGroup.Key,
                       StudentCount=dateGroup.Count()
                       };
            var dtos = await data.AsNoTracking().ToListAsync();
            return View(dtos);
        }

        //public ViewResult Index( )
        //{
        //    IEnumerable<Student> students = _studentRepository.GetAllList();
        //    List<Student> model = _studentRepository.GetAllList().Select(s =>
        //    {
        //        s.EncryptedId = _protector.Protect(s.Id.ToString());
        //        return s;
        //    }).ToList();

        //    return View(model);
        //}
        //     // asp-route-Sorting="@Model.Sorting" asp-route-FilterText="@Model.FilterText"
        public async Task<IActionResult> Index( GetStudentInput input)
        {

            //获取分页的结果
            var dtos = await _studentService.GetPaginatedResult(input);

            dtos.Data = dtos.Data.Select(s =>
            {
                //加密ID值并存储在EncryptedId属性
                s.EncryptedId = _protector.Protect(s.Id.ToString());
                return s;
            }
            ).ToList();

            ////判断searchString ,如果不为空，则去查询参数中的空格
            //ViewBag.CurrentFilter = searchString ?.Trim();
            //PagedResultDto paginationModel = new PagedResultDto();
            ////计算总条数
            //paginationModel.Count = await _studentRepository.CountAsync();
            ////当前页
            //currentPage = 0;
            //paginationModel.CurrentPage = currentPage;
            ////获取分页结果
            //var students = await _studentService.GetPaginatedResult(paginationModel.CurrentPage, searchString, sortBy);
           
            //paginationModel.Data = students.Select(s=>
            //{
            //    s.EncryptedId = _protector.Protect(s.Id.ToString());
            //    return s;
            //}
            //).ToList();

            //IQueryable<Student> query = _studentRepository.GetAll();
            //if (!string.IsNullOrEmpty(searchString))
            //{
            //    //通过模糊查询表中的Name或者Email的值
            //    query = query.Where(s=>s.Name.Contains(searchString)||s.Email.Contains(searchString));
            
            //}
            //query = query.OrderBy(sortBy).AsNoTracking();

            //var model = query.ToList().Select(s =>
            //{
            //    //加密ID值并存储在EncryptedId中
            //    s.EncryptedId = _protector.Protect(s.Id.ToString());
            //    return s;
            //}).ToList();
            return View(dtos);

        }

        private Student DecrytedStudent( string id)
        {
            string decryptedId = _protector.Unprotect(id);

            int decrytedStudentId = Convert.ToInt32(decryptedId);

            Student student = _studentRepository.FirstOrDefault(s => s.Id == decrytedStudentId);
            return student;
        }
        public ViewResult Details(string id)
        {
            var student = DecrytedStudent(id);
            //判断学生信息是否存在
            if (student == null)
            {
                ViewBag.ErrorMessage = $"学生Id={id}的信息不存在，请重试"; 
                return View("NotFound");
            }
            //实例化HomeDetailsViewModel并存储Student详细信息和PageTitle
            HomeDetailsViewModel homeDetailsViewModel = new HomeDetailsViewModel()
            {
                Student = student,
                PageTitle = "学生详情"
            };
            homeDetailsViewModel.Student.EncryptedId = _protector.Protect(student.Id.ToString());

            //将ViewModel对象传递给View()方法
            return View(homeDetailsViewModel);
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        private string ProcessUploadedFile(StudentCreateViewModel model)
        {
            string uniqueFileName = null;
            if (model.Photos.Count>0)
            {
                foreach (IFormFile photo in model.Photos)
                {
                    string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images", "avatars");
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + photo.FileName;
                    string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                    //因为使用了非托管资源，所以需要手动进行释放
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        //使用IFormFile接口提供的CopyTo()方法将文件复制到wwwroot/images/avatars 文件夹
                        photo.CopyTo(fileStream);
                    }
                }
            }
            return uniqueFileName;
        }

        [HttpPost]
        public IActionResult Create(StudentCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var uniqueFileName = ProcessUploadedFile(model);

                Student newStudent = new Student
                {
                    Name = model.Name,
                    Email = model.Email,
                    Major = model.Major,
                    PhotoPath = uniqueFileName,
                    EnrollmentDate = model.EnrollmentDate
                    
                };
                _studentRepository.Insert(newStudent);

                var encryptedId = _protector.Protect(newStudent.Id.ToString());

                return RedirectToAction("Details", new { id = newStudent.Id });
            }
            return View();
        }
        [HttpGet]
        public ViewResult Edit(string id)
        {
           // Student student = _studentRepository.GetStudentById(id);
            var student = DecrytedStudent(id);
            if (student == null)
            {
                ViewBag.ErrorMessage = $"学生Id={id}的信息不存在，请重试";
                return View("NotFound");
            }

            StudentEditViewModel studentEditViewModel = new StudentEditViewModel
            {
                Id = id,
                Name = student.Name,
                Email = student.Email,
                Major = student.Major,
                ExistingPhotoPath = student.PhotoPath,
                EnrollmentDate=student.EnrollmentDate
                
            };
            return View(studentEditViewModel);
        }

        [HttpPost]
        public IActionResult Edit(StudentEditViewModel model)
        {
            //检查提供的数据是否有效，如果没有通过验证，需要重新编辑学生信息
            //这样用户就可以更正并重新提交编辑表单
            if (ModelState.IsValid)
            {
                //从数据库中查询正在编辑的学生信息
                var student = DecrytedStudent(model.Id);

                //用模型对象中的数据更新student对象
                student.Name = model.Name;
                student.Email = model.Email;
                student.Major = model.Major;
                student.EnrollmentDate = model.EnrollmentDate;
                //如果用户想要更改照片，可以上传新照片它会被模型对象上的Photo属性接收
                //如果用户没有上传照片，那么我们会保留现有的照片信息
                //因为兼容了多图上传所有这里的！=null判断修改判断Photos的总数是否大于0
                if (model.Photos != null && model.Photos.Count > 0)
                {
                    //如果上传了新的照片，则必须显示新的照片信息
                    //因此我们会检查当前学生信息中是否有照片，有的话，就会删除它。
                    if (model.ExistingPhotoPath != null)
                    {
                        string filePath = Path.Combine(_webHostEnvironment.WebRootPath, "images", "avatars", model.ExistingPhotoPath);
                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        }
                       
                    }

                    //我们将保存新的照片到 wwwroot/images/avatars  文件夹中，并且会更新
                    //Student对象中的PhotoPath属性，然后最终都会将它们保存到数据库中
                    student.PhotoPath = ProcessUploadedFile(model);
                }

                //调用仓储服务中的Update方法，保存studnet对象中的数据，更新数据库表中的信息。
                Student updatedstudent = _studentRepository.Update(student);

                return RedirectToAction("index");
            }

            return View(model);
        }

        private string ProcessUploadedFile(StudentEditViewModel model)
        {
            string uniqueFileName = null;
            if (model.Photos.Count > 0)
            {
                foreach (var photo in model.Photos)
                {
                    string uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath,"images","avatars");
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + photo.FileName;
                    string filePath = Path.Combine(uploadFolder,uniqueFileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        photo.CopyTo(fileStream);
                    }


                }
            }
            return uniqueFileName;
        }
    }
}
