﻿using SchoolManagement.Models.EnumTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagement.Models
{
    public class Student
    {
        public int Id { get; set; }
       
        public string Name { get; set; }
        
        public MajorEnum? Major { get; set; }
        
        public string Email { get; set; }
     
        public string PhotoPath { get; set; }
        [NotMapped]
        public string EncryptedId { get; set; }
        /// <summary>
        /// 入学时间
        /// </summary>
        public DateTime EnrollmentDate { get; set; }
        public ICollection<StudentCourse> StudentCourses { get; set; }


    }
}
