﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagement.Models
{
    /// <summary>
    /// 学院
    /// </summary>
    public class Department
    {
        public int DepartmentID { get; set; }
        [StringLength(50,MinimumLength =3)]
        public string Name { get; set; }

        /// <summary>
        /// 预算
        /// </summary>
        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal Budget { get; set; }
      
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        public int? TeacherID { get; set; }
        /// <summary>
        /// 学院主任
        /// </summary>
        public Teacher Administrator { get; set; }
        public ICollection<Course> Courses { get; set; }
    }
}
