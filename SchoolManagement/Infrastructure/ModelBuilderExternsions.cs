﻿using Microsoft.EntityFrameworkCore;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace SchoolManagement.Infrastructure
{
    public static class ModelBuilderExternsions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>().ToTable("Course","School");
            modelBuilder.Entity<StudentCourse>().ToTable("StudentCourse","School");
            modelBuilder.Entity<Student>().ToTable("Student","School");

            //modelBuilder.Entity<Student>().HasData(
            //    new Student
            //    {
            //        Id = 1,
            //        Name = "测试人员1",
            //        Major = Models.EnumTypes.MajorEnum.ComputerScience,
            //        Email = "aaaa@qq.com"
            //    }
            //    );
        }
    }
}
