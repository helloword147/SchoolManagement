﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SchoolManagement.Infrastructure.Repositories
{
    /// <summary>
    /// 此接口是所有仓储的约定，此接口仅作为约定，用于标识它们，这里创建了一个泛型的接口
    /// </summary>
    /// <typeparam name="TEntity">当前传入仓储的尸体类型</typeparam>
    /// <typeparam name="TPrimaryKey">传入仓储的主键类型</typeparam>
    public interface IRepository<TEntity, TPrimaryKey> where TEntity : class
    {
        //List<TEntity> GetAllList();//同步方法
        //Task<List<TEntity>> GetAllListAsync();//异步方法
        //List<TEntity> GetAllListAsync(Expression<Func<TEntity, bool>> predicate);//可以支持动态LINQ的同步方法
        ////Task<List<TEntity>> GetAllListAsync(Expression<Func<TEntity, bool>> predicate);//支持传参的异步方法

        #region 查询
        /// <summary>
        /// 获取用于从整个表中检索实体的IQueryTable
        /// </summary>
        /// <returns>可用于从数据库中选择实体</returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// 用于获取所有实体
        /// </summary>
        /// <returns>所有实体列表</returns>
        List<TEntity> GetAllList();
        /// <summary>
        /// 用于获取所有实体的异步实现
        /// </summary>
        /// <returns>所有实体列表</returns>
        Task<List<TEntity>> GetAllListAsync();
        /// <summary>
        /// 用于获取传入本方法的所有实体 <paramref name="predicate">
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>所有实体列表</returns>
        List<TEntity> GetAllList(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// 用于获取传入本方法的所有实体 <paramref name="predicate">
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>所有实体列表</returns>
        Task<List<TEntity>> GetAllListAsync(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// 通过传入的筛选条件来获取实体信息，查询不到返回值，会引发异常
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        TEntity Single(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// 通过传入的筛选条件来获取实体信息，查询不到返回值，会引发异常
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// 通过传入的筛选条件查询实体信息
        /// </summary>
        /// <param name="predicate">筛选条件</param>
        /// <returns></returns>
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// 通过传入的筛选条件查询实体信息，如果没有找到，则返回null
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);
        #endregion

        #region Insert
        /// <summary>
        /// 添加一个实体信息
        /// </summary>
        /// <param name="entity">被添加的实体</param>
        /// <returns></returns>
        TEntity Insert(TEntity entity);
        /// <summary>
        /// 添加一个新实体信息
        /// </summary>
        /// <param name="entity">被添加的实体</param>
        /// <returns></returns>
        Task<TEntity> InsertAsync(TEntity entity);
        #endregion
        #region Update
        /// <summary>
        /// 更新现有实体
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns></returns>
        TEntity Update(TEntity entity);
        /// <summary>
        /// 更新现有实体
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns></returns>
        Task<TEntity> UpdateAsync(TEntity entity);
        #endregion
        #region Delete
        /// <summary>
        /// 删除一个实体
        /// </summary>
        /// <param name="entity">无返回值</param>
        void Delete(TEntity entity);
        /// <summary>
        /// 删除一个实体
        /// </summary>
        /// <param name="entity">无返回值</param>
        /// <returns></returns>
        Task DeleteAsync(TEntity entity);
        /// <summary>
        /// 按传入的实体可删除多个实体，所有符合条件的实体都被检索和删除
        /// 如果条件比较多，则待删除的实体也比较多，这可能会导致主要的性能问题
        /// </summary>
        /// <param name="predicate"></param>
        void Delete(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// 按传入的实体可删除多个实体，所有符合条件的实体都被检索和删除
        /// 如果条件比较多，则待删除的实体也比较多，这可能会导致主要的性能问题
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        Task DeleteAsync(Expression<Func<TEntity, bool>> predicate);
        #endregion
        #region 总和计算

        /// <summary>
        /// 获取此仓储中所有实体的总和
        /// </summary>
        /// <returns></returns>
        int Count();
        /// <summary>
        /// 获取此实体中所有实体的总和
        /// </summary>
        /// <returns></returns>
        Task<int> CountAsync();
        /// <summary>
        /// 支持条件筛选 <paramref name="predicate"/> 计算仓储中的实体总和
        /// </summary>
        /// <param name="predicate">实体的总数</param>
        /// <returns>实体的总数</returns>
        int Count(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// 支持条件筛选 <paramref name="predicate"/> 计算仓储中的实体总和
        /// </summary>
        /// <param name="predicate">实体的总数</param>
        /// <returns>实体的总数</returns>
        Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// 获取此存储库中的所有实体的总和（如果预期返回值大于Int.MaxValue的值，则推荐该方法，简单来说就是返回值类型为long）
        /// <see cref="int.MaxValue">
        /// </summary>
        /// <returns>实体的总数</returns>
        long LoingCount();
        /// <summary>
        /// 获取此存储库中的所有实体的总和（如果预期返回值大于Int.MaxValue的值，则推荐该方法，简单来说就是返回值类型为long）
        /// <see cref="int.MaxValue">
        /// </summary>
        /// <returns>实体的总数</returns>
        Task<long> LongCountAsync();
        /// <summary>
        /// 获取此存储库中的所有实体的总和（如果预期返回值大于Int.MaxValue的值，则推荐该方法，简单来说就是返回值类型为long）
        /// <see cref="int.MaxValue">
        /// </summary>
        /// <returns>实体的总数</returns>
        long LongCount(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// 获取此存储库中的所有实体的总和（如果预期返回值大于Int.MaxValue的值，则推荐该方法，简单来说就是返回值类型为long）
        /// <see cref="int.MaxValue">
        /// </summary>
        /// <returns>实体的总数</returns>
        Task<long> LongCountAsync(Expression<Func<TEntity, bool>> predicate);
        #endregion
    }
}
