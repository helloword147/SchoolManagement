﻿using Microsoft.AspNetCore.Mvc;
using SchoolManagement.CustomerMiddlewares;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagement.ViewModels
{
    public class RegisterViewModel
    {
         [Required]
         [EmailAddress]
         [Display(Name="邮箱地址")]
         [Remote(action:"IsEmailInUse",controller:"Account")]
         [ValidEmailDomain(allowedDomain:"qq.com",ErrorMessage ="邮箱必须是qq邮箱")]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "密码")]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "确认密码")]
        [Compare("Password",ErrorMessage ="两次密码不一致，请重新输入")]
        public string ConfirmPassword { get; set; }

        public string City { get; set; }
        

    }
}
