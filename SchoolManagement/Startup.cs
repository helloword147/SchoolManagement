﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SchoolManagement.DataRepositories;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using SchoolManagement.Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using SchoolManagement.Models;
using SchoolManagement.Security.CustomTokenProvider;
using SchoolManagement.Security;
using SchoolManagement.Infrastructure.Repositories;
using SchoolManagement.Infrastructure.Data;
using SchoolManagement.Application.Students;
using SchoolManagement.Application.Courses;
using NetCore.AutoRegisterDi;

namespace SchoolManagement
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        //配置服务
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddMvc();
            services.AddMvc().AddXmlSerializerFormatters();
            //
            services.AddDbContextPool<AppDbContext>(
                options=>options.UseSqlServer(_configuration.GetConnectionString("StudentDBConnection")));
            
            services.AddControllersWithViews(
                config =>
                {
                    var policy = new AuthorizationPolicyBuilder()
                                              .RequireAuthenticatedUser()
                                              .Build();
                    config.Filters.Add(new AuthorizeFilter(policy));
                }
                )
                .AddXmlSerializerFormatters();
            //services.AddScoped<IStudentRepository, SQLStudentRepository>();
            //services.AddScoped<IStudentService, StudentService>();
            services.RegisterAssemblyPublicNonGenericClasses()
                .Where(c => c.Name.EndsWith("Service"))
                .AsPublicImplementedInterfaces(ServiceLifetime.Scoped);
                
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 3;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(15);
            });

            services.Configure<IdentityOptions>(options =>
            {
                options.SignIn.RequireConfirmedEmail = true;
                //通过自定义的CustomEmailConfirmation名称来覆盖旧的Token名称，
                options.Tokens.EmailConfirmationTokenProvider = "CustomEmailConfirmation";
            });

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders()
                .AddTokenProvider<CustomEmailConfirmationTokenProvider<ApplicationUser>>("CustomEmailConfirmation");

            //策略结合声明授权
            services.AddAuthorization(options =>
            {
                options.AddPolicy("DeleteRolePolicy",
                    policy => policy.RequireClaim("Delete Role"));
                options.AddPolicy("AdminRolePolicy",
                   policy => policy.RequireRole("Admin"));
                //策略结合多个角色进行授权
                options.AddPolicy("SuperAdminPolicy", policy =>
                  policy.RequireRole("Admin", "User", "SuperManager"));

                options.AddPolicy("EditRolePolicy", policy => policy.RequireClaim("Edit Role"));

            });
            services.AddAuthorization(options=>
            {
                options.AddPolicy("EditRolePolicy",
                    policy=>policy.RequireClaim("Edit Role","true"));

            });
            services.AddScoped<ICourseService, CourseService>();
            services.ConfigureApplicationCookie(options=>
            {
                options.AccessDeniedPath=new PathString("/Admin/AccessDenied");
            });
            services.AddAuthentication().AddMicrosoftAccount(microsoftOptions=>
            {
                microsoftOptions.ClientId = _configuration["Authentication:Microsoft:ClientId"];
                microsoftOptions.ClientSecret = _configuration["Authentication:Microsoft:ClientSecret"];
            }).AddGitHub(options=> 
            {
                options.ClientId = _configuration["Authentication:GitHub:ClientId"];
                options.ClientSecret = _configuration["Authentication:GitHub:ClientSecret"];

            });
            //修改所有令牌类型的有效期为5小时
            services.Configure<DataProtectionTokenProviderOptions>(o=>o.TokenLifespan=TimeSpan.FromHours(5));
            //修改电子邮箱验证令牌类型的有效期为3天
            services.Configure<CustomEmailConfirmationTokenProviderOptions>(o=>o.TokenLifespan=TimeSpan.FromDays(3));
            services.AddSingleton<DataProtectionPurposeStrings>();
            services.AddTransient(typeof(IRepository<,>),typeof(RepositoryBase<,>));


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //配置管道
        //目前有两个中间件在管道中UseDeveloperExceptionPage()方法和Run()方法
        //ILogger < Startup >被注入到Configure()方法中
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDataInitializer();
            //如果环境是Development serve Developer Exception Page
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //else提供具有应用程序支持的用户友好错误页面联系信息
            else if (env.IsStaging() || env.IsProduction() || env.IsEnvironment("UAT"))
            {
                app.UseExceptionHandler("/Error");
                app.UseStatusCodePagesWithReExecute("/Error/{0}");
            }

         
            //使用纯静态文件支持的中间件，而不使用带有终端中间件
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseRouting();
            //授权中间件
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
            

        }
    }
}
