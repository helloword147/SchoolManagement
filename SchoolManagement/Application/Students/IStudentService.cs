﻿using SchoolManagement.Application.Dtos;
using SchoolManagement.Application.Students.Dtos;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SchoolManagement.Application.Students
{
    public interface IStudentService
    {
       // Task<List<Student>> GetPaginatedResult(int currentPage, string searchString, string sortBy, int pageSize = 10);
        Task<PagedResultDto<Student>> GetPaginatedResult(GetStudentInput input);
    }
}
