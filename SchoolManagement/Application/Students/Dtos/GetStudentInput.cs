﻿using SchoolManagement.Application.Dtos;
using System;

namespace SchoolManagement.Application.Students.Dtos
{
    public class GetStudentInput : PagedSortedAndFilterInput
    {
        public GetStudentInput()
        {
            Sorting = "Id";
        }

    }
}