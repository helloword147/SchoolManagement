﻿using Microsoft.Extensions.Logging;
using SchoolManagement.Infrastructure;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagement.DataRepositories
{
    public class SQLStudentRepository : IStudentRepository
    {
        private readonly AppDbContext _context;
        private ILogger logger;
        public SQLStudentRepository(AppDbContext context,ILogger<SQLStudentRepository> logger)
        {
            _context = context;
            this.logger = logger;
        }
        public Student Add(Student student)
        {
            throw new NotImplementedException();
        }

        public Student Delete(int id)
        {
            Student student = _context.Students.Find(id);
            if (student != null)
            {
                _context.Students.Remove(student);
                _context.SaveChanges();
            }
            return student;
        }

        public IEnumerable<Student> GetAllStudents()
        {
            //logger.LogTrace("学生信息 TraceLog");
            //logger.LogDebug("学生信息 Debug");
            //logger.LogInformation("学生信息 信息");
            //logger.LogWarning("学生信息 警告");
            //logger.LogError("学生信息 错误");
            //logger.LogCritical("学生信息 严重");
            return _context.Students;
        }

       

        public Student GetStudentById(int id)
        {
            return _context.Students.Find(id);
        }

        public Student Insert(Student student)
        {
            _context.Students.Add(student);
            _context.SaveChanges();
            return student;
        }

        public Student Update(Student UpdateStudent)
        {
            var student = _context.Students.Attach(UpdateStudent);
            student.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();
            return UpdateStudent;
        }
    }
}
