﻿using SchoolManagement.Models;
using SchoolManagement.Models.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagement.DataRepositories
{
    public class MockStudentRepository : IStudentRepository
    {
        private List<Student> _studentList;
        public MockStudentRepository()
        {
            _studentList = new List<Student>()
          {
            new Student() { Id = 1, Name = "张三", Major = MajorEnum.ComputerScience, Email = "zhangsan@52abp.com" },
            new Student() { Id = 2, Name = "李四", Major = MajorEnum.Mathematics, Email = "lisi@52abp.com" },
            new Student() { Id = 3, Name = "赵六", Major = MajorEnum.ElectronicCommerce, Email = "zhaoliu@52abp.com" },
            };
        }

        public IEnumerable<Student> GetAllStudents()
        {
            return _studentList;
        }

        public Student GetStudent(int id)
        {
            return _studentList.FirstOrDefault(a => a.Id == id);
        }

        public Student Add(Student student)
        {
            student.Id = _studentList.Max(s=>s.Id)+1;
            _studentList.Add(student);
            return student;
        }
        public Student Delete( int id)
        {
            Student student = _studentList.FirstOrDefault(s=>s.Id==id);
            if (student != null)
            {
                _studentList.Remove(student);
            }
            return student;
        }
        public Student GetStudentById(int id)
        {
            return _studentList.FirstOrDefault(a => a.Id == id);

        }

        public Student Insert( Student student)
        {
            student.Id = _studentList.Max(s => s.Id) + 1;
            _studentList.Add(student);
            return student;
        }

        public Student Update(Student Updatestudent)
        {
            Student student = _studentList.FirstOrDefault(s => s.Id == Updatestudent.Id);
            if (student != null)
            {
                student.Name = Updatestudent.Name;
                student.Email = Updatestudent.Email;
                student.Major = Updatestudent.Major;

            }
            return student;
        }
    }
}
