﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolManagement.Migrations
{
    public partial class removeSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Student_Student_StudentId",
                table: "Student");

            migrationBuilder.DropIndex(
                name: "IX_Student_StudentId",
                table: "Student");

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Student");

            migrationBuilder.RenameTable(
                name: "StudentCourse",
                newName: "StudentCourse",
                newSchema: "School");

            migrationBuilder.RenameTable(
                name: "Student",
                newName: "Student",
                newSchema: "School");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "StudentCourse",
                schema: "School",
                newName: "StudentCourse");

            migrationBuilder.RenameTable(
                name: "Student",
                schema: "School",
                newName: "Student");

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Student",
                type: "int",
                nullable: true);

            migrationBuilder.InsertData(
                table: "Student",
                columns: new[] { "Id", "Email", "EnrollmentDate", "Major", "Name", "PhotoPath", "StudentId" },
                values: new object[] { 1, "aaaa@qq.com", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "测试人员1", null, null });

            migrationBuilder.CreateIndex(
                name: "IX_Student_StudentId",
                table: "Student",
                column: "StudentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Student_Student_StudentId",
                table: "Student",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
