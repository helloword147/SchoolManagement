﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolManagement.Migrations
{
    public partial class addallschoolentiies2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DepatmentID",
                schema: "School",
                table: "Course");

            migrationBuilder.RenameColumn(
                name: "Bugget",
                table: "Departments",
                newName: "Budget");

            migrationBuilder.AlterColumn<int>(
                name: "DepartmentID",
                schema: "School",
                table: "Course",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Budget",
                table: "Departments",
                newName: "Bugget");

            migrationBuilder.AlterColumn<int>(
                name: "DepartmentID",
                schema: "School",
                table: "Course",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "DepatmentID",
                schema: "School",
                table: "Course",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
