﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagement.Security.CustomTokenProvider
{
    public class DataProtectionPurposeStrings
    {
        public readonly string StudentIdRouteValue = "StudentIdRouteValue";
    }
}
